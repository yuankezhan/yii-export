<?php

namespace yuankezhan\yiiExport\controls;

class FileOperate
{
    public static function readDirsFiles(string $dirPath, $verifyFunc = "")
    {
        // 获取目录下的所有文件
        $filename = scandir($dirPath);
        $filenameList = [];
        foreach ($filename as $name) {
            // 跳过两个特殊目录
            if ($name == "." || $name == "..") {
                continue;
            }
            //获取需要删除的文件
            if (empty($verifyFunc) || $verifyFunc($name)) {
                $filenameList[] = "{$dirPath}/{$name}";
            }
        }
        return $filenameList;
    }

    public static function delFiles(array $filesList)
    {
        foreach ($filesList as $deleteFilename) {
            if (!file_exists($deleteFilename)) {
                continue;
            }
            if (is_file($deleteFilename)) {
                unlink($deleteFilename);
            } else {
                $secondList = FileOperate::readDirsFiles($deleteFilename);
                FileOperate::delFiles($secondList);
                rmdir($deleteFilename);
            }
        }
    }

    /*注意！$dir 为文件的绝对路径*/
    public static function deldir(string $dir)
    {
        //删除目录下的文件：
        $dh = opendir($dir);
        while ($file = readdir($dh)) {
            if ($file != "." && $file != "..") {
                $fullpath = $dir . "/" . $file;
                if (!is_dir($fullpath)) {
                    unlink($fullpath);
                } else {
                    FileOperate::deldir($fullpath);
                }
            }
        }
        closedir($dh);
    }
}