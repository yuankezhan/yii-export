<?php
namespace yuankezhan\yiiExport\controls;

class Result
{
    public $Success;
    public $Data;
    public $Message;

    /**
     * Result constructor.
     * @param $Success
     * @param $Data
     * @param $Message
     */
    public function __construct($Success, $Data, $Message)
    {
        $this->Success = $Success;
        $this->Data = $Data;
        $this->Message = $Message;
    }
}