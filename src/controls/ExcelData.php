<?php
namespace yuankezhan\yiiExport\controls;

class ExcelData
{
    public $Count;
    public $List;

    public function __construct(int $count, array $list)
    {
        $this->Count = $count;
        $this->List = $list;
    }
}