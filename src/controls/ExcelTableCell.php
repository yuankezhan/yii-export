<?php
namespace yuankezhan\yiiExport\controls;

class ExcelTableCell
{
    public $Value;
    public $Row;
    public $Site;

    /**
     * ExcelTableCell constructor.
     * @param string $value 内容
     * @param string $row   列
     * @param string $site  坐标
     */
    public function __construct(string $value = null, string $row = null, string $site = null)
    {
        $this->Value = $value;
        $this->Row = strtoupper($row);
        $this->Site = strtoupper($site);
    }
}