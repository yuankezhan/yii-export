<?php

namespace yuankezhan\yiiExport\controls;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class FileOperation
{
    private $RootDirectory = "/upload/";
    private $ExcelTxtSaveFile = "excelTxt";
    private $ExcelSaveFile = "excel";

    public function writeTxt(string $jsonData)
    {
        $pathInfo = $this->_createDirectory($this->ExcelTxtSaveFile);
        $thisTxtPath = $pathInfo['absolutePath'] . "/" . uniqid() . "_txt.txt";
        $thisTxtFile = fopen($thisTxtPath, "w") or die("Unable to open file!");
        fwrite($thisTxtFile, $jsonData);
        fclose($thisTxtFile);
        return new Result(true, $thisTxtPath, "");
    }

    public function integrationExcel(array $txtPathList, int $startingPosition)
    {
        $dataList = $this->_integrationExcelData($txtPathList);
        $pathInfo = $this->_createDirectory($this->ExcelSaveFile);
        $fileName = uniqid() . ".xls";
        $thisExcelPath = $pathInfo['absolutePath'] . "/" . $fileName;
        $thisDownloadPath = $pathInfo['relativePath'] . "/" . $fileName;

        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheep = $spreadsheet->getActiveSheet();

        $newIndex = $startingPosition;
        foreach ($dataList as $itemList) {
            $override = true;
            foreach ($itemList as $item) {
                $pCoordinate = $this->_getCoordinateAndValue($item, $newIndex, $override);
                $sheep->setCellValue($pCoordinate, $item->Value);
            }
        }
        $generalWriter = IOFactory::createWriter($spreadsheet, 'Xls');
        $generalWriter->save($thisExcelPath);
        $data = [
            ["name" => $fileName, "href" => $thisDownloadPath]
        ];
        return new Result(true, $data, "");
    }

    private function _getCoordinateAndValue($data, int &$index, &$override)
    {
        if (!empty($data->Site)) {
            return $data->Site;
        } else {
            if ($override) {
                $index++;
                $override = false;
            }
            return ($data->Row . $index);
        }
    }

    private function _integrationExcelData(array $txtPathList)
    {
        $dataInfo = [];
        foreach ($txtPathList as $txtPath) {
            if (empty($txtPath)) {
                continue;
            }
            $thisList = json_decode($this->_getFileStr($txtPath));
            $dataInfo = array_merge($dataInfo, $thisList);
        }
        return $dataInfo;
    }

    private function _getFileStr(string $filePath)
    {
        if (file_exists($filePath)) {
            $fp = fopen($filePath, "r");
            $str = fread($fp, filesize($filePath));
            fclose($fp);
            return $str;
        }
        return null;
    }

    private function _createDirectory(string $filename)
    {
        $aRootDirectory = $_SERVER['DOCUMENT_ROOT'] . $this->RootDirectory;
        if (!is_dir($aRootDirectory)) {
            mkdir($aRootDirectory);
            chmod($aRootDirectory, 0777);
        }

        $relativePath = $this->RootDirectory . $filename;
        $absolutePath = $_SERVER['DOCUMENT_ROOT'] . $relativePath;
        if (!is_dir($absolutePath)) {
            mkdir($absolutePath);
            chmod($absolutePath, 0777);
        }

        $relativePath = $relativePath. "/" . date("Ymd");
        $absolutePath = $_SERVER['DOCUMENT_ROOT'] . $relativePath;
        if (!is_dir($absolutePath)) {
            mkdir($absolutePath);
            chmod($absolutePath, 0777);
        }
        return ["relativePath" => $relativePath, "absolutePath" => $absolutePath];
    }

    /**
     * @param string $filePath 父级 文件夹路径
     * @param int $holdTime 保存时间/天
     * @return Result
     */
    public function deleteExcelFileByDate(int $holdTime)
    {
        $filePath = $_SERVER['DOCUMENT_ROOT'] . $this->RootDirectory;
        $txtFilePath = $filePath .  $this->ExcelTxtSaveFile;
        $excelFilePath = $filePath .  $this->ExcelSaveFile;
        if (!file_exists($txtFilePath) && !file_exists($excelFilePath)) {
            return new Result(true, true, "删除文件成功");
        }
        // 获取目录下满足条件的文件
        $filenameList1 = FileOperate::readDirsFiles($txtFilePath, function ($name) use ($holdTime) {
            return is_numeric($name) && $name <= date("Ymd") - $holdTime;
        });
        $filenameList2 = FileOperate::readDirsFiles($excelFilePath, function ($name) use ($holdTime) {
            return is_numeric($name) && $name <= date("Ymd") - $holdTime;
        });
        $filenameList = array_merge($filenameList1, $filenameList2);
        FileOperate::delFiles($filenameList);
        return new Result(true, true, "删除文件成功");
    }

}