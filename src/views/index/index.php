<?php
use yii\helpers\Url;
?>

<div id="export-create-<?=$name?>"></div>

<script>
    var <?=$name?> = new Export ({
        dataOperate: '<?=Url::to(["index/method-data-operate"])?>',
        downloadUrl: '<?=Url::to(["index/trigger-download"])?>',
        deleteTxtUrl: '<?=Url::to(["index/delete-txt-path"])?>',
        deleteExcelUrl: '<?=Url::to(["index/delete-excel-by-hold-time"])?>',
        ele: '#export-create-<?=$name?>',
        methodUrl: '<?=$methodUrl?>',
        pageSize: <?=$maxQuantity?>,
        startingPosition: <?=$startingPosition?>
    });
</script>