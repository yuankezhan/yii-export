<?php
namespace yuankezhan\yiiExport;

use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle
{
    /**
     * {@inheritdoc}
     */
    public $sourcePath = __DIR__ . '/assets';
    public $cssOptions = ['position' => View::POS_HEAD];
    public $jsOptions = ['position' => View::POS_HEAD];

    /**
     * {@inheritdoc}
     */
    public $css = [
        'css/style.css',
    ];

    /**
     * {@inheritdoc}
     */
    public $js = [
        'js/jquery-3.6.0.min.js',
        'js/export.js'
    ];
}