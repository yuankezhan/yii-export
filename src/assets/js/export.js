(function (window, document) {
    var Export = function (options) {

        if (!(this instanceof Export)) {
            return new Export(options);
        }

        this.localValue = {
            dataOperate: null,
            downloadUrl: null,
            deleteTxtUrl: null,
            deleteExcelUrl: null,
            holdTime: 3,
            ele: 'body',
            pageIndex: 0,
            startingPosition: 1,
            pageSize: 100,
            methodUrl: null,
            errorIndex: 0,
            everySchedule: 1,
            txtPathList: []
        };

        this.opt = this.extend(this.localValue, options, true);
        this.initDom();
    };

    Export.prototype = {
        extend: function (o, n, override) {
            for (var key in n) {
                if (n.hasOwnProperty(key) && (!o.hasOwnProperty(key) || override)) {
                    o[key] = n[key];
                }
            }
            return o;
        },
        initDom: function () {
            this.createProgressBar();
            this.getDataByLink();
        },
        createProgressBar: function () {
            $(this.opt.ele).html('<div class="generate_progress">\n' +
                '    <div class="generate_progress_c">\n' +
                '        <div class="generate_test_title">文件生成中，请稍候 >>></div>\n' +
                '        <div class="progress progress_l">\n' +
                '            <div class="progress-bar progress-bar-striped active progress_bar_l" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">\n' +
                '                <span class="show_schedule_l">0%</span>\n' +
                '            </div>\n' +
                '        </div>\n' +
                '    </div>\n' +
                '</div>');
        },
        /*写入txt*/
        processMethodUrlData: function (data) {
            var _that = this;
            var thisData = data.length > 0 ? JSON.stringify(data) : '';

            $.ajax({
                url: _that.opt.dataOperate,
                type: 'post',
                dataType: 'json',
                data : {
                    dataList: thisData
                },
                success: function (res) {
                    if (!res.Success) {
                        alert(res.Message);
                    } else {
                        _that.opt.txtPathList.push(res.Data);
                        _that.requestResult(res.Data);
                    }
                },
                error: function (res) {
                    console.log(res, '写入时出现了一次错误！');
                    if (_that.opt.errorIndex < 3) {
                        _that.opt.errorIndex++;
                        _that.processMethodUrlData(data);
                    }
                }
            });
        },
        /*请求数据*/
        getDataByLink: function () {
            var _that = this;
            $.ajax({
                url: _that.opt.methodUrl,
                type: 'get',
                dataType: 'json',
                data : {
                    pageIndex: _that.opt.pageIndex,
                    pageSize: _that.opt.pageSize
                },
                success: function (res) {
                    if (!res.Success) {
                        alert(res.Message);
                    } else {
                        _that.requestResultVerdict(res.Data);
                    }
                },
                error: function (res) {
                    console.log(res, '请求数据出现了一次错误！');
                    if (_that.opt.errorIndex < 3) {
                        _that.opt.errorIndex++;
                        _that.getDataByLink();
                    }
                }
            });
        },
        /*触发下载*/
        downloadFun: function (downloadList) {
            for (var i = 0; i < downloadList.length; i++) {
                var elemIF = document.createElement('a');
                elemIF.href = encodeURI(downloadList[i]['href']);
                elemIF.setAttribute('download', downloadList[i]['name']);
                elemIF.style.display = 'none';
                document.body.appendChild(elemIF);
                $(elemIF)[0].click();
            }
        },
        /*下载*/
        triggerDownload: function () {
            var _that = this;
            $.ajax({
                url: _that.opt.downloadUrl,
                type: 'post',
                dataType: 'json',
                data : {
                    txtPathList: _that.opt.txtPathList,
                    startingPosition: _that.opt.startingPosition
                },
                success: function (res) {
                    console.log(res);
                    if (!res.Success) {
                        alert(res.Message);
                        return;
                    }
                    _that.downloadFun(res.Data);
                    _that.deleteTxtPath();
                },
                error: function (res) {
                    console.log(res, '写入数据出现了一次错误！');
                    if (_that.opt.errorIndex < 3) {
                        _that.opt.errorIndex++;
                        _that.triggerDownload();
                    } else {
                        alert("xls(Excel5) 最大行数65535 最大列数 255，请注意！");
                    }
                }
            });

        },
        /*数据处理*/
        requestResultVerdict: function (data) {
            if (this.opt.pageIndex === 1) {
                if (data.Count > 0) {
                    this.opt.everySchedule = this.opt.pageSize / data.Count;
                    this.deleteExcelUrl();
                }
            }
            this.processMethodUrlData(data.List);
        },
        resultTrue: function () {
            this.opt.pageIndex++;
            this.opt.errorIndex = 0;
        },
        /*结果处理*/
        requestResult: function (data) {
            if (data == null || data === '') {
                this.triggerDownload();
                this.adjustingProgressBar(true);
                return;
            }
            this.adjustingProgressBar(false);
            this.resultTrue();
            this.getDataByLink();
        },
        getPercent: function () {
            let percent = (this.opt.pageIndex) * (this.opt.everySchedule) * 100;
            if (percent >= 100) {
                return 100;
            }
            return percent;
        },
        adjustingProgressBar: function (finish) {
            var percentStr = Math.floor(this.getPercent()) + '%';
            if (finish) {
                $(this.opt.ele + ' .generate_test_title').html('完成，正在创建下载 >>>');
            }
            $(this.opt.ele + ' .progress_bar_l').css('width', percentStr);
            $(this.opt.ele + ' .show_schedule_l').html(percentStr);
        },
        /*数据处理成功后删除txt文件*/
        deleteTxtPath: function () {
            var _that = this;
            $.ajax({
                url: _that.opt.deleteTxtUrl,
                type: 'post',
                dataType: 'json',
                data : {
                    txtPathList: _that.opt.txtPathList
                },
                success: function (res) {
                    if (!res.Success) {
                        alert(res.Message);
                    }
                }
            });
        },
        /*定期清理excel和未删除的txt，释放服务器内存，默认三天*/
        deleteExcelUrl: function () {
            var _that = this;
            $.ajax({
                url: _that.opt.deleteExcelUrl,
                type: 'get',
                dataType: 'json',
                data : {
                    holdTime: _that.opt.holdTime
                },
                success: function (res) {
                    if (!res.Success) {
                        alert(res.Message);
                    }
                }
            });
        }
    };

    window.Export = Export;
})(window, document);