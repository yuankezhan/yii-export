<?php

namespace yuankezhan\yiiExport\controllers;

use yuankezhan\yiiExport\controls\FileOperate;
use yuankezhan\yiiExport\controls\FileOperation;
use yuankezhan\yiiExport\controls\Result;

class IndexController extends BaseController
{
    public $layout = 'main';

    public function actionIndex($methodUrl, $maxQuantity = 100, $startingPosition = 1, $name = "deriveIndex")
    {
        return $this->render('index', [
            "name" => $name,
            "maxQuantity" => $maxQuantity,
            "startingPosition" => $startingPosition,
            "methodUrl" => $methodUrl
        ]);
    }

    public function actionMethodDataOperate()
    {
        $jsonData = \Yii::$app->request->post("dataList", null);
        if (empty($jsonData)) {
            return json_encode(new Result(true, null, ""));
        }
        $obj = new FileOperation();
        $txtPathRes = $obj->writeTxt($jsonData);
        if (!$txtPathRes->Success) {
            return json_encode(new Result(false, null, $txtPathRes->Message));
        }
        return json_encode(new Result(true, $txtPathRes->Data, ""));
    }

    public function actionTriggerDownload()
    {
        $txtPathList = \Yii::$app->request->post("txtPathList", []);
        $startingPosition = \Yii::$app->request->post("startingPosition", 2);

        if (empty($txtPathList)) {
            return json_encode(new Result(false, [], "没有要写入的数据！"));
        }
        $obj = new FileOperation();
        $res = $obj->integrationExcel($txtPathList, $startingPosition);
        if (!$res->Success) {
            return json_encode(new Result(false, [], $res->Message));
        }
        return json_encode(new Result(true, $res->Data, ""));
    }

    public function actionDeleteExcelByHoldTime($holdTime)
    {
        $obj = new FileOperation();
        $res = $obj->deleteExcelFileByDate($holdTime);
        return json_encode($res);
    }

    public function actionDeleteTxtPath()
    {
        $txtPathList = \Yii::$app->request->post("txtPathList", []);
        FileOperate::delFiles($txtPathList);
        return json_encode(new Result(true, null, ""));
    }

}